# Maintainer: Azkali Manad <a.ffcc7@gmail.com>

pkgbase="FEX"
pkgname="FEX"
pkgver="2108"
pkgrel=1.0
pkgdesc='Fast x86 emulation frontend'
arch=('aarch64')
url='https://github.com/FEX-Emu/FEX'
license=('GPL3')
makedepends=('cmake' 'make' 'git' 'clang' 'sdl2' 'glfw-x11' 'libepoxy' 'meson' 'ninja' 'x86_64-elf-gcc')
provides=("${pkgname%}")
conflicts=("${pkgname%}" 'FEX')
source=('git+https://github.com/FEX-Emu/FEX.git#branch=FEX-2108')
md5sums=('SKIP')

prepare() {
	cd "$srcdir/FEX"
	git submodule update --init	
}

build() {
	cd "$srcdir/FEX"

	CC=clang CXX=clang++ \
	cmake . \
		-DCMAKE_BUILD_TYPE=Release \
		-DENABLE_LTO=True \
		-DBUILD_TESTS=False \
		-DBUILD_THUNKS=True \
		-G Ninja \
		-DCMAKE_INSTALL_PREFIX:PATH=/usr

	ninja -j$(nproc)
}

_package() {
	ninja DESTDIR="$pkgdir/" install -C "$srcdir/FEX"
}

_package-binfmt_misc_32() {
	install=FEX.install
	ninja DESTDIR="$pkgdir/" binfmt_misc_32 -C "$srcdir/FEX"
}

_package-binfmt_misc_64() {
	install=FEX.install
	ninja DESTDIR="$pkgdir/" binfmt_misc_64 -C "$srcdir/FEX"
}

_package_thunks() {
	mkdir -p "$pkgdir/opt/FEX/"
	cp -r "$srcdir/FEX/Host" "$pkgdir/opt/FEX/"
	cp -r "$srcdir/FEX/Guest" "$pkgdir/opt/FEX/"
}

pkgname=(
	"${pkgbase}"
	"${pkgbase}-binfmt_misc_32"
	"${pkgbase}-binfmt_misc_64"
	"${pkgbase}-thunks"
)

for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done
