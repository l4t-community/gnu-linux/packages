#!/bin/bash
usage() {
	echo "./build.sh <flavor> <package_name>"
}

arch() {
	# Register qemu binaries
	docker run --rm --privileged aptman/qus -- -r
	docker run --rm --privileged aptman/qus -s -- -c -p

	# Build build container
	docker build \
		-t alarm-builder:latest \
		-f dockerfiles/Dockerfile.arch \
		dockerfiles/

	# Build pkg
	docker run \
		--rm \
		-v $PWD/pkgbuilds/$1:/mnt/src \
		-v $PWD/pkgbuilds/$1/tmp:/mnt/src_cache \
		alarm-builder:latest
}

if [[ -z "$1" || -z "$2" ]]; then
	usage; exit 1;
fi

$1 $2
